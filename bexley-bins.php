<?php
// create a new cURL resource
$ch = curl_init();

// set URL and other appropriate options
curl_setopt($ch, CURLOPT_URL, "https://services.athomeapp.net/ServiceData/GetUserRoundJson");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	"X-country: gb",
	"X-udprn: 1234",
	"X-email: whatever@example.com"
));

$json = curl_exec($ch);

$data = json_decode($json, true);

$today = new DateTime("now");

$when = null;
$output = "";

foreach ($data["userrounds"] as $key) {
	//	Trim useless text
	$containerName = str_replace(" box", "", $key["containername"]);
	$nextCollectionDateString = $key["nextcollectiondates"][0]["datestring"];
	//	"datestring": "18 10 2019 12:00",
	$date = DateTime::createFromFormat('d m Y G:i', $nextCollectionDateString);
	$interval = date_diff($today, $date);
	$days = $interval->format('%d');

	if ($when == null && $days == 0) {
		$when = "Today's collection is. ";
	} else if ($when == null) {
		$dayName = $date->format('l');
		$when = "{$dayName}'s collection is. ";
	}

	//	Ignore garden waste
	if (strpos($containerName, 'garden') == false && $days <= 7) {
		$output .= "{$containerName}. ";
	}
}


$speech = "{$when} {$output}";

//  Set the correct header for JSON data
header('Content-Type: application/json');
//  Setup a response
$response = [
	"response" => [
		"outputSpeech" => [
			"type" => "PlainText",
			"text" => $speech
		]
	]
];

// Return the output
echo json_encode($response);

// close cURL resource, and free up system resources
curl_close($ch);

die();
